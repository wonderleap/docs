---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Quickstart for Developers

## First Steps

We're so excited for you to start monetizing your WebXR creation with us.

Please note that we currently only support [A-FRAME](https://aframe.io/), which is one of the most popular web frameworks for building virtual reality experiences. If you'd like to see support for other frameworks, let us know!

## Placing the ad

Placing the ad in your WebXR content is simple.

In the HTML `<head>`, place the following `script` tag _after_ the AFRAME script:

```html
<script src="https://lib.wonderleap.co/releases/latest/wonderleap.js"></script>
```

Then in your A-FRAME scene, place the ad:

```html
<a-entity visible="true" wonder-ad="auId: $YOUR_AUID_HERE" position="0 2 0"></a-entity>
```

Here is an example implementation of our ad. It's very important that you import A-Frame before the ad.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>WebVR</title>
    <meta name="description" content="WebVR">
    <script src="https://aframe.io/releases/0.9.2/aframe.min.js"></script>
    <script src="https://cdn.rawgit.com/donmccurdy/aframe-extras/v6.0.0/dist/aframe-extras.min.js"></script>
    <script src="https://unpkg.com/aframe-environment-component@1.1.0/dist/aframe-environment-component.min.js"></script>
    <script src="https://lib.wonderleap.co/releases/latest/wonderleap.js"></script>
  </head>
  <body>
    <a-scene id="scene" environment="preset: tron" stats cursor="rayOrigin: mouse"
             inspector="https://cdn.jsdelivr.net/gh/aframevr/aframe-inspector@master/dist/aframe-inspector.min.js">

      <a-entity id="rig" rotation="0 0 0" movement-controls>
        <!-- Player -->
        <a-entity id="camera" camera look-controls wasd-controls="fly: true; acceleration: 100"
            position="0 2 5"></a-entity>

        <!-- Hand controls -->
        <a-entity hand-controls="right" laser-controls raycaster="far: 100"></a-entity>
      </a-entity>

      <!-- Box to block the ad -->
      <a-box color="tomato" depth="0.5" height="4" width="4" position="0 2 2"></a-box>
      <a-box color="tomato" depth="4" height="4" width="0.5" position="2.5 2 2"></a-box>

      <!-- Our ads -->
      <a-entity visible="true" wonder-ad="auId: ce6f68fc-4809-4409-8f57-c631283ce5a3" position="0 2 0"></a-entity>
    </a-scene>
  </body>
</html>
```

### A Tip

We recommend using the [A-FRAME Visual Inspector Dev Tool](https://github.com/aframevr/aframe-inspector) to correctly place the ad in the right place.
